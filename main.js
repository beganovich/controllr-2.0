const restify = require('restify'); 
const robot = require('robotjs'); 

const server = restify.createServer();


server.get('/', (req, res) => {
    let body = `

        <html>
            <button id='next'>Next</button>
            <button id='previous'>Previous</button>
        </html>
        <script>
            document.getElementById('next').onclick = () => fetch('/slide/next'); 
            document.getElementById('previous').onclick = () => fetch('/slide/previous');
        </script>`
    
    res.writeHead(200, {
        'Content-Length': Buffer.byteLength(body),
        'Content-Type': 'text/html'
      });
      res.write(body);
      res.end();
})

server.get('/slide/next', (req, res) => {
    console.log('slide/next')
    robot.keyTap('pagedown');
    res.json({message: 'slide/next'}); 
    res.end();
})

server.get('/slide/previous', (req, res) => {
    console.log('slide/previous');
    robot.keyTap('pageup'); 
    res.json({message: 'slide/previous'}); 
    res.end();
})

server.listen(8080, () => console.log('Up @ 8080'));